#include <iostream>
#include <random>
#include <chrono>
#include <thread>

using namespace std;

void calc_subpi(long& counter, long N)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dist(0, 1);


    for (int i = 0 ; i < N ; ++i)
    {
        double x = dist(gen);
        double y = dist(gen);
        if ( x*x + y*y < 1)
            ++counter;
    }
}

int main()
{    
    int n_of_threads = thread::hardware_concurrency();
    cout << "Avail threads = " <<  n_of_threads << endl;

    auto start = chrono::high_resolution_clock::now();

    long N = 10000000000;
    vector<long> counter(n_of_threads, 0);
    //long counter = 0;
    vector<thread> threads;
    for (int i = 0 ; i < n_of_threads ; ++i)
        threads.emplace_back(calc_subpi, ref(counter[i]), N/n_of_threads);
        //threads.emplace_back(calc_subpi, ref(counter), N/n_of_threads);

    for(auto& th : threads) th.join();
    auto end = chrono::high_resolution_clock::now();

    cout << "Elapsed = " << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
    cout << "Pi = " << double(accumulate(counter.begin(), counter.end(), 0L))/double(N) * 4 << endl;

    //cout << "Pi = " << double(counter)/double(N) * 4 << endl;


    return 0;
}

