#include <iostream>
#include <thread>
#include <vector>
#include "../../utils.h"

using namespace std;

void hello()
{
    cout << "Hello from thread" << endl;
}

void say_id(int id)
{
    cout << "Hello from thread " << id << endl;
}

int main()
{
    cout << "moveable threads" << endl;
    thread th1(hello);
    cout << "is th1 joinable? " << th1.joinable() << endl;
    thread th2 = move(th1);

    cout << "is th2 joinable? " << th2.joinable() << endl;
    cout << "is th1 joinable? " << th1.joinable() << endl;
    th2.join();

    vector<scoped_thread> threads;
    threads.push_back(thread(hello));
    threads.emplace_back(hello);
    threads.emplace_back(say_id, 10);

    //for(auto &th : threads) th.join();

    scoped_thread th10(thread(hello));
    scoped_thread th11(say_id, 11);

    return 0;
}

