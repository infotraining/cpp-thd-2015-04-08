#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <atomic>

using namespace std;

volatile long counter = 0;
mutex mtx;
atomic<long> cnt_atomic{0};

class RAIImutex
{
    mutex& mtx_;
public:
    RAIImutex(mutex& mtx ) : mtx_(mtx)
    {
        mtx_.lock();
    }

    ~RAIImutex()
    {
        mtx_.unlock();
    }

    RAIImutex(const RAIImutex&) = delete;
    RAIImutex& operator=(const RAIImutex&) = delete;
};

void increment()
{
    for(int i = 0 ; i < 100000 ; ++i)
    {
        ++counter;
    }
}

void increment_mtx()
{
    for(int i = 0 ; i < 100000 ; ++i)
    {
        lock_guard<mutex> r(mtx);
        ++counter;
        if (counter == 13) return;
    }
}

void increment_atomic()
{
    for(int i = 0 ; i < 100000 ; ++i)
    {
        cnt_atomic.fetch_add(1);
    }
}

int main()
{
    vector<thread> thds;

    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 2 ; ++i)
        thds.emplace_back(increment_mtx);
    for(auto& th : thds) th.join();

    auto end = chrono::high_resolution_clock::now();

    cout << "Elapsed mtx = " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    cout << "Counter = " << counter << endl;

    thds.clear();
    counter = 0;
    start = chrono::high_resolution_clock::now();
    for (int i = 0 ; i < 2 ; ++i)
        thds.emplace_back(increment);
    for(auto& th : thds) th.join();

    end = chrono::high_resolution_clock::now();

    cout << "Elapsed no-mtx = " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    cout << "Counter = " << counter << endl;

    thds.clear();
    start = chrono::high_resolution_clock::now();
    for (int i = 0 ; i < 2 ; ++i)
        thds.emplace_back(increment_atomic);
    for(auto& th : thds) th.join();

    end = chrono::high_resolution_clock::now();

    cout << "Elapsed atomic = " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    cout << "Counter = " << cnt_atomic.load() << endl;

    return 0;
}

