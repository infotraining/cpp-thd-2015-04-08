#include <iostream>
#include <thread>

using namespace std;

void hello()
{
    cout << "Hello from thread" << endl;
}

void say_id(int id)
{
    cout << "Hello from thread " << id << endl;
}

struct Task
{
    void operator()(double value)
    {
        cout << "Task is called with " << value << endl;
    }
};

void some_fun(int& a)
{
    this_thread::sleep_for(chrono::milliseconds(100));
    ++a;
    cout << "some fun called " << a<< endl;
}

void const_fun(const int& b)
{
    this_thread::sleep_for(chrono::milliseconds(100));
    cout << "const_fun with " << b << endl;
}

thread generate_thread()
{
    //int a = 3;
    //thread th( [a] () mutable { some_fun(a); });
    int b = 10;
    thread th(const_fun, b);
    return th;
}

int main()
{
    cout << "Hello World!" << endl;
    thread th1(&say_id, 10);
    th1.join();
    Task t;
    thread th2(t, 3.14);
    th2.join();
    thread th3( [] () { cout << "Hello from lambda" << endl;});
    th3.join();
    int a = 3;
    thread th4(some_fun, ref(a));
    th4.join();
    thread th5 = generate_thread();
    th5.join();
    return 0;
}

