#ifndef UTILS_H
#define UTILS_H
#include <thread>
#include <mutex>
#include <atomic>
#include <queue>
#include <condition_variable>
#include <functional>
#include <future>

class scoped_thread
{
    std::thread th_;
public:
    scoped_thread(const scoped_thread&) = delete;
    scoped_thread& operator=(const scoped_thread&) = delete;

    scoped_thread(scoped_thread&& rval) // = default <- works in gcc
    {
        th_ = std::move(rval.th_);
    }
    scoped_thread& operator=(scoped_thread&& rval)
    {
        th_ = std::move(rval.th_);
        return *this;
    }

    template<typename... T>
    scoped_thread(T&&... args) : th_(std::forward<T>(args)...)
    {
    }

    scoped_thread(std::thread th) : th_(std::move(th))
    {
    }

    ~scoped_thread()
    {
        if (th_.joinable())
            th_.join();
    }
};

template <typename T>
class thread_safe_queue
{
    std::queue<T> q;
    std::mutex mtx;
    std::condition_variable cond;
public:
    void push(const T& item)
    {
        std::lock_guard<std::mutex> lg(mtx);
        q.push(item);
        cond.notify_one();
    }

    void push(T&& item)
    {
        std::lock_guard<std::mutex> lg(mtx);
        q.push(std::move(item));
        cond.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> ul(mtx);
        while(q.empty())
            cond.wait(ul);
        item = std::move(q.front());
        q.pop();
    }

    bool pop_nowait(T& item)
    {
        std::lock_guard<std::mutex> ul(mtx);
        if(q.empty())
            return false;
        item = std::move(q.front());
        q.pop();
        return true;
    }
};

using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> q;
    std::vector<scoped_thread> workers;

public:
    thread_pool(int n_of_workers)
    {
        for (int i = 0 ; i < n_of_workers ; ++i)
            workers.emplace_back(&thread_pool::worker, this);
    }

    void submit(task_t task)
    {
        if (task)
            q.push(task);
    }

    void worker()
    {
        for(;;)
        {
            task_t task;
            q.pop(task);
            if(!task) return;
            task();
        }
    }

    template<typename F>
    auto async(F f) -> std::future<decltype(f())>
    {
        using res_t = decltype(f());
        auto pt = std::make_shared<std::packaged_task<res_t()>>(f);
        std::future<res_t> res = pt->get_future();
        q.push([pt] { (*pt)();});
        return res;
    }

    ~thread_pool()
    {
        for (int i = 0 ; i < workers.size() ; ++i)
            q.push(task_t());
    }
};

#endif // UTILS_H

