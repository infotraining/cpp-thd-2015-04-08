#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include <queue>
#include <condition_variable>
#include "../../utils.h"
#include <functional>
#include <fstream>

using namespace std;

class Logger
{
    ofstream f;
    thread_pool ao{1};

public:
    Logger(std::string fname) {
        f.open(fname);
    }
    void log(std::string message)
    {
        ao.submit([message, this] { f << message << endl; } );
    }
};

void log_data(string message, Logger& log)
{
    for (int i = 0 ; i < 1000 ; ++i)
    {
        log.log(message + to_string(i));
    }
}

int main()
{
    Logger log("test.txt");
    scoped_thread th1(log_data, "message type 1 ", ref(log));
    scoped_thread th2(log_data, "message type 2 ", ref(log));
    return 0;
}

