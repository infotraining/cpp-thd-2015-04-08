#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include <queue>
#include <condition_variable>
#include "../../utils.h"
#include <functional>

using namespace std;

void can_throw(int id, exception_ptr& ptr)
{
    try{
        cout << "Worker " << id << endl;
        this_thread::sleep_for(1s);
        if(id == 13) throw std::runtime_error("bad luck");
    }
    catch(...)
    {
        cerr << "exception in worker" << endl;
        ptr = current_exception();
    }
}

int can_throw_simple(int id)
{
    cout << "Worker " << id << endl;
    this_thread::sleep_for(1s);
    if(id == 13) throw std::runtime_error("bad luck");
    return 42;
}

int main()
{
    exception_ptr ptr;
    future<int> res = async(launch::async, can_throw_simple, 13);
    res.wait();
    try{
        scoped_thread(can_throw,14, ref(ptr));
        if (ptr)
        {
            rethrow_exception(ptr);
        }

        cout << "res " << res.get();
    }
    catch(...)
    {
        cerr << "exception occured" << endl;
    }

    return 0;
}

