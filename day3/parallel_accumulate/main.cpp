#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include <queue>
#include <condition_variable>
#include "../../utils.h"
#include <future>

using namespace std;

template<typename It, typename T>
future<T> parallel_accumulate(It begin, It end, T init)
{
    int n_of_threads = thread::hardware_concurrency();
    size_t size = distance(begin, end);
    size_t block_size = size / n_of_threads;

    It block_start = begin;

    vector<shared_future<T>> results;
    for(int i = 0 ; i < n_of_threads ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if (i == n_of_threads-1 ) block_end = end;
        results.push_back(async(launch::async,
                                &accumulate<It, T>,
                                block_start,
                                block_end,
                                T()));
        block_start = block_end;
    }

    // return future...

    // return async(launch::deferred, [results = move(results)] () mutable c++14 only
    return async(launch::deferred, [results] ()
    {
        T res{};
        for (auto & f : results) res += f.get();
        return res;
    });

//    return accumulate(results.begin(), results.end(), init,
//                      [] (const T& a, future<T>& b) { return a + b.get(); });
}

int main()
{
    vector<double> v;
    long N = 100000000;
    for (int i = 0 ; i < N ; ++i)
        v.push_back(i);

    auto start = chrono::high_resolution_clock::now();

    cout << "Sum = " << accumulate(v.begin(), v.end(), 0L) << endl;
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    start = chrono::high_resolution_clock::now();
    future<long> res = parallel_accumulate(v.begin(), v.end(), 0L);
    cout << "Sum parallel = " << res.get() << endl;
    end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
    return 0;
}

