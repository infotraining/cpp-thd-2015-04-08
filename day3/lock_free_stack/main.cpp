#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include <queue>
#include <condition_variable>
#include "../../utils.h"
#include <functional>

using namespace std;

struct Node
{
    Node* prev;
    int data;
};

class Stack
{
    atomic<Node*> head {nullptr};
public:
    void push(int data)
    {
        Node* new_node = new Node;
        new_node->data = data;
        new_node->prev = head.load();
        while(!head.compare_exchange_weak(new_node->prev, new_node));  // logical error
    }

    int pop()
    {
        Node* old_node = head.load();
        while(!head.compare_exchange_weak(old_node, old_node->prev));
        int tmp = old_node->data;
        delete old_node;
        return tmp;
    }
};

void test_stack(Stack& s)
{
    for (int i = 0 ; i < 100 ; ++i)
        s.push(i);
    for (int i = 0 ; i < 100 ; ++i)
        cout << s.pop() << ", ";
    cout << endl;
}

int main()
{
    Stack s;
    scoped_thread th1(test_stack, ref(s));
    scoped_thread th2(test_stack, ref(s));
    return 0;
}

