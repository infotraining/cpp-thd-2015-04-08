#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include <queue>
#include <condition_variable>
#include "../../utils.h"
#include <functional>

using namespace std;

class Service
{
public:
    virtual void run() = 0;
    virtual ~Service() {}
};

class RealService : public Service
{
    string url_;
public:
    RealService(const string& url) : url_(url)
    {
        cout << "Creating service" << endl;
        this_thread::sleep_for(2s);
        cout << "Service is ready" << endl;
    }

    void run()
    {
        cout << "RealService::run" << endl;
    }
};

namespace bad {

class ProxyService : public Service
{
    RealService* real_service{nullptr};
    string url_;
    mutex mtx;
public:
    ProxyService(const string& url) : url_(url)
    {}

    void run()
    {
        if (real_service == nullptr)
        {
            lock_guard<mutex> lg(mtx);
            if( real_service == nullptr)
                real_service = new RealService(url_);
        }
        real_service->run();
    }
    ~ProxyService()
    {
        delete real_service;
    }
};

}

namespace atom {

class ProxyService : public Service
{
    atomic<RealService*> real_service{nullptr};
    string url_;
    mutex mtx;
public:
    ProxyService(const string& url) : url_(url)
    {}

    void run()
    {
        if (real_service == nullptr)
        {
            lock_guard<mutex> lg(mtx);
            if( real_service.load() == nullptr)
                real_service.store(new RealService(url_));
        }
        (*real_service).run();
    }
    ~ProxyService()
    {
        delete real_service;
    }
};

}

namespace call_onuce {

class ProxyService : public Service
{
    RealService* real_service{nullptr};
    string url_;
    once_flag flag;
public:
    ProxyService(const string& url) : url_(url)
    {}

    void run()
    {
         call_once(flag, [this] () { real_service = new RealService(url_);});
         real_service->run();
    }

    ~ProxyService()
    {
        delete real_service;
    }
};

}

namespace staticmember {

class ProxyService : public Service
{
    static RealService real_service;
    string url_;
public:
    ProxyService(const string& url) : url_(url)
    {}

    RealService get_instance()
    {
        static RealService real_service(url_);
        return real_service;
    }

    void run()
    {
         get_instance().run();
    }
};

}


using namespace staticmember;

int main()
{
    ProxyService service("www.wp.pl");
    scoped_thread th1(&ProxyService::run, &service);
    scoped_thread th2(&ProxyService::run, &service);
    return 0;
}

