#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include <queue>
#include <condition_variable>
#include "../../utils.h"
#include <future>

using namespace std;

int question()
{
    this_thread::sleep_for(1s);
    return 42;
}

class my_packaged_task
{
    std::function<int()> f_;
    std::promise<int> p;
public:
    my_packaged_task(std::function<int()> f) : f_(f)
    {}
    void operator ()()
    {
        p.set_value(f_());
    }

    std::future<int> get_future()
    {
        return p.get_future();
    }
};

int main()
{
//    int result;
//    thread th1( [&result] { result = question();} );
//    th1.join();
//    cout << "Result = " << result << endl;

    vector<future<int>> futures;
    for (int i = 0 ; i < 10 ; ++i)
        futures.push_back(async(launch::async, question));

    cout << "Starting calculations" << endl;

    for (auto& res : futures)
        cout << "Result = " << res.get() << endl;

    cout << "**************************" << endl;

    my_packaged_task pt(question);
    future<int> pt_result = pt.get_future();
    thread pt_thread(move(pt));
    pt_thread.join();

    cout << "Result = " << pt_result.get() << endl;
    return 0;
}

