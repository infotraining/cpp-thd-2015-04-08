#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include "../../utils.h"

using namespace std;

mutex mtx;
timed_mutex tmtx;

void worker_deprecated()
{
    cout << "Worker started " << this_thread::get_id() << endl;
    for(;;)
    {
        if(mtx.try_lock())
        {
            cout << "I've got mutex " << this_thread::get_id() << endl;
            this_thread::sleep_for(2s);
            mtx.unlock();
            return;
        }
        else
        {
            cout << "Waiting for mutex " << this_thread::get_id() << endl;
            this_thread::sleep_for(200ms);
        }
    }
}

void worker()
{
    cout << "Worker started " << this_thread::get_id() << endl;
    unique_lock<timed_mutex> ul(tmtx, try_to_lock);
    if(!ul.owns_lock())
    {
        do
        {
            cout << "Waiting for mutex " << this_thread::get_id() << endl;
        }
        while (!ul.try_lock_for(200ms));
    }
    cout << "I've got mutex " << this_thread::get_id() << endl;
    this_thread::sleep_for(2s);
    return;
}

int main()
{
    scoped_thread th1(worker);
    scoped_thread th2(worker);
    return 0;
}

