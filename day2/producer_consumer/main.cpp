#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include <queue>
#include <condition_variable>
#include "../../utils.h"

using namespace std;

queue<int> q;
mutex mtx;
condition_variable cond;

template<typename T1, typename T2>
auto add(const T1& a, const T2& b) -> decltype(a+b)
{
    return a+b;
}

void producer()
{
    for(int i = 1 ; i < 200 ; ++i)
    {
        {
            lock_guard<mutex> lg(mtx);
            q.push(i);
            cond.notify_one();
        }
        this_thread::sleep_for(100ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> ul(mtx);

        while(q.empty())
            cond.wait(ul);
        //cond.wait(ul, [] { return !q.empty();});

        int msg = q.front();
        q.pop();
        cout << id << " got " << msg << endl;

    }
}

auto main() -> int
{
    scoped_thread prod(producer);
    scoped_thread cons1(consumer, 1);
    scoped_thread cons2(consumer, 2);
    return 0;
}

