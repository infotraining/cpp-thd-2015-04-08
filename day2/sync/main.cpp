#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include "../../utils.h"

using namespace std;

class Data
{ -O2
    vector<int> data_;
    atomic<bool> is_ready{false};
public:
    void read()
    {
        data_.resize(20);
        cout << "reading data" << endl;
        cout << "is my bool really lock_free? " << is_ready.is_lock_free() << endl;
        generate(data_.begin(), data_.end(), [] {return rand() % 100;});
        this_thread::sleep_for(60s);
        is_ready = true;
    }

    void process()
    {
        for(;;)
        {
            if(is_ready)
            {
                cout << "processing " << endl;
                cout << "sum = " << accumulate(data_.begin(), data_.end(), 0) << endl;
                break;
            }
            //this_thread::sleep_for(1us);
            this_thread::yield();
        }
    }
};

int main()
{
    cout << "Hello World!" << endl;
    Data data;
    scoped_thread th_read(&Data::read, &data);
    scoped_thread th_process1(&Data::process, &data);
    scoped_thread th_process2(&Data::process, &data);
    return 0;
}

