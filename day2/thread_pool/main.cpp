#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include <queue>
#include <condition_variable>
#include "../../utils.h"
#include <functional>

using namespace std;

void run_task(function<void()> f)
{
    f();
}

void hello()
{
    cout << "Hello" << endl;
}

struct Task
{
    void operator()()
    {
        cout << "Functor" << endl;
    }
};

int question()
{
    this_thread::sleep_for(1s);
    return 42;
}

int main()
{
    cout << "Hello World!" << endl;
    run_task(hello);
    run_task( [] { cout << "lambda" << endl;});
    run_task(Task());
    function<void()> f;
    f = hello;
    f = function<void()>();
    if (!f)
    {
        cout << "Not a function" << endl;
    }

    thread_pool tp(4);

    future<int> res = tp.async(question);

    tp.submit(hello);
    tp.submit(Task());
    for (int i = 0 ; i < 10 ; ++i)
    {
        tp.submit([i] { cout << "Lambda nr " << i << endl; });
    }

    return 0;
}

