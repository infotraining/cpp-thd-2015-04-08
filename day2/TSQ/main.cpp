#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <atomic>
#include <queue>
#include <condition_variable>
#include "../../utils.h"

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for(int i = 1 ; i < 200 ; ++i)
    {
        q.push(i);
        this_thread::sleep_for(100ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        int msg;
        q.pop(msg);
        cout << id << " got " << msg << endl;
    }
}

auto main() -> int
{
    scoped_thread prod(producer);
    scoped_thread cons1(consumer, 1);
    scoped_thread cons2(consumer, 2);
    return 0;
}

